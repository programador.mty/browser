import {
    BrowserWindow, 
    ipcMain,
    dialog,
    app,
    BrowserView, 
} from 'electron'
import path from 'path'

/**
 * 
 * !No borrar la carpeta dist
 * 
 * Los archivos de Typescript se combierten a JavaScript y se almacenan en la carpeta dist, 
 *      Si necesitas modificar html, css directo en la carpeta dist
 */

///<------------- creación de ventana de windows
const createWindows = () => {
    let bandera = false
    const win = new BrowserWindow({ ///<------------- creación de objeto para ventana
        width: 800, height: 600,
        title: "TyT contact center",
        backgroundColor: '#2F8496',
        icon: path.join(__dirname, '/source/logo.ico'),
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            preload: path.join(__dirname, '/html/renderer.js'), ///<------------- cargado de JavaScript en el DOM 
            webviewTag: true
        },
        frame: false
    })
    win.loadFile(path.join(__dirname, '/html/index.html'));
    /// win.webContents.openDevTools() <------------- habilita la consola de Chrome

    ipcMain.on('CHANGEURL', (_, URL) => { ///<------------- funciona como tipo socket, es para la ejecucion de funciones desde el JS del DOM
        const view = new BrowserView({ ///<------------- creación de ventana para abrir la URL buscada
            webPreferences: {
              nodeIntegration: false,
            }
        });
        view.webContents.loadURL(URL as any);
        win.setBrowserView(view);
        win.getBrowserView()?.setBounds({ x: 0, y: 37, width: 800, height: (600 - 37) }) ///<------------- posición de la ventana
        win.getBrowserView()?.setAutoResize({ width: true, height: true}); ///<------------- se ajusta ancho y largo de la ventana adicional
    })

    win.on('close', (e) => {
        const _e = e
        try {
            if(!bandera){
                _e.preventDefault()
                const choice = dialog.showMessageBox(
                    win,
                    {
                        type: 'question',
                        buttons: ['Yes', 'No'],
                        title: 'Confirmación se cierre',
                        message: 'Estas seguro de cerrar la pestaña ?',
                    }
                ).then((t) => t.response)
                choice.then((t) => {
                    if(!t){
                        bandera = true
                        win.close()
                    }
                })
            }
        } catch (error) {
            console.error(error)
        }
    })
}

app.whenReady().then(() => {
    createWindows() ///<------------- iniciar browser

    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) createWindows() ///<------------- iniciar browser
    })
})

app.on('window-all-closed', () =>  app.quit() ) ///<------------- habilitar el cerrado de la ventana de windows