import {remote, ipcRenderer} from 'electron'

function validateIPaddress(ipaddress : string) {
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|:.*[0-9]/.test(ipaddress)) 
        return true
    return false
}

const win = remote.getCurrentWindow();

document.onreadystatechange = (event) => {
    if (document.readyState == "complete") {
        handleWindowControls();
    }
};

window.onbeforeunload = (event) => {
    win.removeAllListeners();
}

function handleWindowControls() {

    document.getElementById('input-browser-change')
        ?.addEventListener("keydown", event => {
            const value = (event.target as HTMLInputElement).value
            if (value.length > 5 && event.key === "Enter"/* && validateIPaddress(value)*/) {
                ipcRenderer.send('CHANGEURL', `https://${value}`) ///<------------- envio de URL
            }
        })

    document.getElementById('min-button')
        ?.addEventListener("click", event => {
            win.minimize();
        });

    document.getElementById('max-button')
        ?.addEventListener("click", event => {
            win.maximize();
        });

    document.getElementById('restore-button')
        ?.addEventListener("click", event => {
            win.unmaximize();
        });

    document.getElementById('close-button')
        ?.addEventListener("click", event => {
            win.close();
        });

    toggleMaxRestoreButtons();
    win.on('maximize', toggleMaxRestoreButtons);
    win.on('unmaximize', toggleMaxRestoreButtons);

    function toggleMaxRestoreButtons() {
        if (win.isMaximized()) {
            document
                .body
                .classList
                .add('maximized');
        } else {
            document
                .body
                .classList
                .remove('maximized');
        }
    }
}