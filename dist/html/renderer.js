"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
function ValidateIPaddress(ipaddress) {
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|:.*[0-9]/.test(ipaddress))
        return true;
    return false;
}
const win = electron_1.remote.getCurrentWindow();
document.onreadystatechange = (event) => {
    if (document.readyState == "complete") {
        handleWindowControls();
    }
};
window.onbeforeunload = (event) => {
    win.removeAllListeners();
};
function handleWindowControls() {
    var _a, _b, _c, _d, _e;
    (_a = document.getElementById('input-browser-change')) === null || _a === void 0 ? void 0 : _a.addEventListener("keydown", event => {
        const value = event.target.value;
        if (value.length > 5 && event.key === "Enter") {
            electron_1.ipcRenderer.send('CHANGEURL', `https://${value}`);
        }
    });
    (_b = document.getElementById('min-button')) === null || _b === void 0 ? void 0 : _b.addEventListener("click", event => {
        win.minimize();
    });
    (_c = document.getElementById('max-button')) === null || _c === void 0 ? void 0 : _c.addEventListener("click", event => {
        win.maximize();
    });
    (_d = document.getElementById('restore-button')) === null || _d === void 0 ? void 0 : _d.addEventListener("click", event => {
        win.unmaximize();
    });
    (_e = document.getElementById('close-button')) === null || _e === void 0 ? void 0 : _e.addEventListener("click", event => {
        win.close();
    });
    toggleMaxRestoreButtons();
    win.on('maximize', toggleMaxRestoreButtons);
    win.on('unmaximize', toggleMaxRestoreButtons);
    function toggleMaxRestoreButtons() {
        if (win.isMaximized()) {
            document
                .body
                .classList
                .add('maximized');
        }
        else {
            document
                .body
                .classList
                .remove('maximized');
        }
    }
}
//# sourceMappingURL=renderer.js.map