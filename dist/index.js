"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const path_1 = __importDefault(require("path"));
const createWindows = () => {
    let bandera = false;
    const win = new electron_1.BrowserWindow({
        width: 800, height: 600,
        title: "TyT contact center",
        backgroundColor: '#2F8496',
        icon: path_1.default.join(__dirname, '/source/logo.ico'),
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            preload: path_1.default.join(__dirname, '/html/renderer.js'),
            webviewTag: true
        },
        frame: false
    });
    win.loadFile(path_1.default.join(__dirname, '/html/index.html'));
    electron_1.ipcMain.on('CHANGEURL', (_, URL) => {
        var _a, _b;
        const view2 = new electron_1.BrowserView({
            webPreferences: {
                nodeIntegration: false,
            }
        });
        view2.webContents.loadURL(URL);
        win.setBrowserView(view2);
        (_a = win.getBrowserView()) === null || _a === void 0 ? void 0 : _a.setBounds({
            x: 0, y: 37, width: 800, height: (600 - 37)
        });
        (_b = win.getBrowserView()) === null || _b === void 0 ? void 0 : _b.setAutoResize({ width: true, height: true });
    });
    win.on('close', (e) => {
        const _e = e;
        try {
            if (!bandera) {
                _e.preventDefault();
                const choice = electron_1.dialog.showMessageBox(win, {
                    type: 'question',
                    buttons: ['Yes', 'No'],
                    title: 'Confirmación se cierre',
                    message: 'Estas seguro de cerrar la pestaña ?',
                }).then((t) => t.response);
                choice.then((t) => {
                    if (!t) {
                        bandera = true;
                        win.close();
                    }
                });
            }
        }
        catch (error) {
            console.error(error);
        }
    });
};
electron_1.app.whenReady().then(() => {
    createWindows();
    electron_1.app.on('activate', function () {
        if (electron_1.BrowserWindow.getAllWindows().length === 0)
            createWindows();
    });
});
electron_1.app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
});
//# sourceMappingURL=index.js.map